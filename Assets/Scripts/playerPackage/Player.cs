﻿using System.Collections;
using common;
using common.objectPooling;
using enemy.asteroidPackage;
using enemy.UFOPackage;
using UnityEngine;

namespace playerPackage
{
    public class Player : ScreenLimitation
    {
        [SerializeField] private ParticleSystem engineParticle;
        [SerializeField] private Transform shootingPos;
        
        private GameManager gameManager;
        private LineRenderer lineRend;
        
        private float simpleShotDelay;
        private float lazerShotDelay;
        
        private void Start()
        {
            lineRend = GetComponent<LineRenderer>();
            gameManager = GameManager.Instance;
            
            InIt(
                0,
                gameManager.GetPlayerRotatedSpeed,
                gameManager.GetPlayerShootingSpeed,
                shootingPos);
        }

        private void FixedUpdate()
        {
            Move(movementSpeed);
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                engineParticle.Play();
                movementSpeed += gameManager.GetPlayerIncreaseSpeed;
            }
            else
            {
                engineParticle.Stop();
                movementSpeed -= movementSpeed > 0 ? gameManager.GetPlayerIncreaseSpeed : 0;
            }

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                Rotate(rotatedSpeed);
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                Rotate(rotatedSpeed * -1);
        }

        private void Update()
        {
            CheckPosition();
            Shooting();
        }

        protected override void Move(float speed) => transform.Translate(Vector3.up * speed * Time.deltaTime);
        // protected override void Move(float speed) => rb.AddRelativeForce(Vector3.up * speed * Time.deltaTime);
        protected override void Rotate(float dir) => transform.Rotate(new Vector3(0, 0, dir * Time.deltaTime));
        protected override void Shooting()
        {
            if (Input.GetKey(KeyCode.K))
                SimpleShot();
            else if (Input.GetKeyDown(KeyCode.L))
                LazerShot();

            simpleShotDelay -= Time.deltaTime;
            lazerShotDelay -= Time.deltaTime;
        }

        protected internal override void Death()
        {
            GameManager.Instance.StopGame();
            gameObject.SetActive(false);
        }

        public void Respawn()
        {
            transform.position = new Vector3(0 ,0,0);
            transform.rotation = new Quaternion(0 ,0,0,0);
            movementSpeed = 0;
            gameObject.SetActive(true);
        }

        private void SimpleShot()
        {
            if (simpleShotDelay <= 0)
            {
                simpleShotDelay = GameManager.Instance.GetPlayerDelaySimpleShooting;
                var bullet = PoolManager.Instance.GetObject("PlayerBullet", bulletSpawnPosition.position, transform.rotation);
                bullet.GetComponent<Rigidbody2D>().AddForce(transform.up * shootingForse, ForceMode2D.Force);
            }
        }

        private void LazerShot()
        {
            if (lazerShotDelay <= 0)
            {
                lazerShotDelay = GameManager.Instance.GetPlayerDelayLazerShooting;
                StartCoroutine(Lazer(0.5f));
            }
        }

        private IEnumerator Lazer(float shotTime)
        {
            var timer = 0f;
            while (timer < shotTime)
            {
                var ray = new Ray2D(shootingPos.position, shootingPos.up * 10);
                var hit = Physics2D.Raycast(ray.origin, ray.direction);

                lineRend.SetPosition(0, ray.direction * 100);
                lineRend.SetPosition(1, ray.origin);
                
                if (hit.collider != null)
                { 
                    lineRend.SetPosition(0, hit.point);
                    switch (hit.collider.tag)
                    {
                        case "Obstacle":
                            hit.collider.GetComponent<Asteroid>().Death();
                            PoolManager.Instance.CheckPool();
                            break;
                        case "UFO":
                            hit.collider.GetComponent<UFO>().Death();
                            PoolManager.Instance.CheckPool();
                            break;
                    }
                }
                yield return new WaitForSeconds(Time.deltaTime);
                timer += Time.deltaTime;
            }
            
            lineRend.SetPosition(0, Vector3.zero); 
            lineRend.SetPosition(1, Vector3.zero); 
            yield return null;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("EnemyBullet") || other.CompareTag("UFO") || other.CompareTag("Obstacle"))
                Death();
        }
    }
}
    