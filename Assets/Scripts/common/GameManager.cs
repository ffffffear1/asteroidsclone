﻿using common.singleton;
using common.stateMachine;
using common.stateMachine.gameplayStates;
using playerPackage;
using UI;
using UnityEngine;

namespace common
{
    public class GameManager : Singleton<GameManager>
    {
        [Header("Player settings:")] 
        [Tooltip("Value of the player's speed increase")]
        [Range(0.001f, 0.1f)]
        [SerializeField] private float playerIncreaseSpeed;
        [Range(80, 200)]
        [SerializeField] private float playerRotatedSpeed;
        [Range(50, 200)]
        [Tooltip("Speed at which the player's bullet will be fired")]
        [SerializeField] private float playerShootingSpeed;
        [Range(0.2f, 2)]
        [Tooltip("Delay between player shots")]
        [SerializeField] private float playerDelaySimpleShooting;
        [Range(3, 12)]
        [Tooltip("Delay between player lazer-shots")]
        [SerializeField] private float playerDelayLazerShooting;


        [Header("Asteroid settings:")]
        [Tooltip("Speed settings of the asteroid are scaled relative to its size so a large asteroid will have 30% of the value middle 30% small 30%")]
        [Range(2, 8)]
        [SerializeField] private float asteroidMovementSpeed;
        [Tooltip("Rotation settings of the asteroid are scaled relative to its size so a large asteroid will have 30% of the value middle 30% small 30%")]
        [Range(5, 40)]
        [SerializeField] private float asteroidRotatedSpeed;
        
        [Header("UFO settings:")] 
        [Tooltip("Value of the UFO's speed increase")]
        [Range(0.005f, 0.02f)]
        [SerializeField] private float UFOIncreaseSpeed;
        [Range(20, 100)]
        [Tooltip("Speed at which the UFO's bullet will be fired")]
        [SerializeField] private float UFOShootingSpeed;
        [Range(0.5f, 5)]
        [Tooltip("Delay between UFO shots")]
        [SerializeField] private float UFODelayShooting;
        
        [Header("Generator settings:")] 
        [Tooltip("Number of asteroids appearing at the beginning of the game")]
        [Range(3, 10f)]
        [SerializeField] private int countFirstSpawnAsteroids;
        [Range(1, 10)]
        [Tooltip("Number of asteroids added after the round")]
        [SerializeField] private int increaseSpawnAsteroids;
        [Tooltip("UFO spawn rate")]
        [Range(10, 30f)]
        [SerializeField] private int rateSpawnUFO;
        
        private float score, roundCount;
        private Player player;
        private StateMachine stateMachine;
        private State Menu, Play, Lose;

        public float GetPlayerIncreaseSpeed => playerIncreaseSpeed;
        public float GetPlayerRotatedSpeed => playerRotatedSpeed;
        public float GetPlayerShootingSpeed => playerShootingSpeed;
        public float GetPlayerDelaySimpleShooting => playerDelaySimpleShooting;
        public float GetPlayerDelayLazerShooting => playerDelayLazerShooting;
        public float GetAsteroidMovementSpeed => asteroidMovementSpeed;
        public float GetAsteroidRotatedSpeed => asteroidRotatedSpeed;
        public float GetUfoIncreaseSpeed => UFOIncreaseSpeed;
        public float GetUfoShootingSpeed => UFOShootingSpeed;
        public float GetUfoDelayShooting => UFODelayShooting;
        public int GetCountFirstSpawnAsteroids => countFirstSpawnAsteroids;
        public int GetIncreaseSpawnAsteroids => increaseSpawnAsteroids;
        public int GetRateSpawnUfo => rateSpawnUFO;
        public Player GetPlayer => player;

        private void Awake()
        {
            player = FindObjectOfType<Player>();

            stateMachine = new StateMachine();
            Menu = new MenuState();
            Play = new GameplayState();
            Lose = new LoseState();

            stateMachine.Init(Menu);
        }

        public void StartGame() => stateMachine.ChangeState(Play);
        public void StopGame()
        {
            StopAllCoroutines();
            stateMachine.ChangeState(Lose);
        }

        public void RestartGame()
        {
            stateMachine.ChangeState(Menu);
        }
        
        public float Score
        {
            get => score;
            set { 
                score = value;
                UIController.Instance.OnScoreChanged(score); 
            }
        }
        public float RoundCount
        {
            get => roundCount;
            set => roundCount = value;
        }
    }
}
