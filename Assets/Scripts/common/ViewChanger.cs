﻿using System.Collections.Generic;
using common.objectPooling;
using common.singleton;
using UnityEngine;

namespace common
{
    public class ViewChanger : Singleton<ViewChanger>
    {
        public GameObject view2D, view3D;

        public List<View> objects;

        private void Start()
        {
            objects = null;
            InIt();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                ChangeAllView();
        }

        private void InIt()
        {
            if (objects == null)
            {
                objects = new List<View>();
                objects.Add(GameManager.Instance.GetPlayer.GetComponent<View>());
                
                for (int i = 0; i < PoolManager.Instance.parent.transform.childCount; i++)
                {
                    if (PoolManager.Instance.parent.transform.GetChild(i).GetComponent<View>() != null)
                        objects.Add(PoolManager.Instance.parent.transform.GetChild(i).GetComponent<View>());
                }
            }
        }
        
        private void ChangeAllView()
        {
            view3D.SetActive(!view3D.activeSelf);
            view2D.SetActive(!view2D.activeSelf);
            
            foreach (var changeObject in objects) { changeObject.ChangeView(); }
        }
    }
}