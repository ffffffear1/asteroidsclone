﻿using UnityEngine;

namespace common
{
    public class View : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private GameObject object3D;

        public void ChangeView()
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
            object3D.SetActive(!object3D.activeSelf);
        }
    }
}