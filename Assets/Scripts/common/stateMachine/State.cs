﻿﻿﻿namespace common.stateMachine
{
    public class State
    {
        private StateMachine stateMachine; 
        public virtual void Enter() { }
        public virtual void Exit() { }
    }
}
