﻿﻿﻿ namespace common.stateMachine
{
    public class StateMachine
    {
        private State curState { get; set; }
        public void Init(State startState)
        {
            curState = startState;
            curState.Enter();
        }
        public void ChangeState(State newState)
        {
            curState.Exit();
            curState = newState;
            curState.Enter();
        }
    }
}