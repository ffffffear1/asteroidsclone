﻿using common.objectPooling;
using UI;

namespace common.stateMachine.gameplayStates
{
    public class GameplayState : State
    {
        public override void Enter()
        {
            UIController.Instance.gamePlayPanel.SetActive(true);
            PoolManager.Instance.CheckPool();
            
            base.Enter();
        }

        public override void Exit()
        {
            UIController.Instance.gamePlayPanel.SetActive(false);
            
            base.Exit();
        }
    }
}