﻿using common.objectPooling;
using UI;

namespace common.stateMachine.gameplayStates
{
    public class LoseState : State
    {
        public override void Enter()
        {
            UIController.Instance.finalScore.text = $"YOUR SCORE: {GameManager.Instance.Score}";
            UIController.Instance.losePanel.SetActive(true);
            
            base.Enter();
        }

        public override void Exit()
        {
            for (int i = 0; i < PoolManager.Instance.parent.transform.childCount; i++) 
            { PoolManager.Instance.parent.transform.GetChild(i).GetComponent<PoolObject>().ReturnToPool(); }
            
            GameManager.Instance.Score = 0;
            GameManager.Instance.RoundCount = 0;
            UIController.Instance.losePanel.SetActive(false);
            
            base.Exit();
        }
    }
}