﻿using UI;

namespace common.stateMachine.gameplayStates
{
    public class MenuState : State
    {
        public override void Enter()
        {
            GameManager.Instance.GetPlayer.Respawn();
            UIController.Instance.startPanel.SetActive(true);
            
            base.Enter();
        }

        public override void Exit()
        {
            UIController.Instance.startPanel.SetActive(false);
            
            base.Exit();
        }
    }
}