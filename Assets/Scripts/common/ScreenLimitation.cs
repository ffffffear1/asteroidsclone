﻿using UnityEngine;

namespace common
{
    public abstract class ScreenLimitation : Controller
    {
        private float posX, posY;
        private float borderX = 9, borderY = 5;
        
        protected void CheckPosition()
        {
            posX = camera.WorldToViewportPoint(transform.position).x;
            posY = camera.WorldToViewportPoint(transform.position).y;
            
            if (posX > 1.02)
                transform.position = new Vector3(borderX * -1, transform.position.y);
            else if (posX < -0.02)
                transform.position = new Vector3(borderX, transform.position.y);
            
            if (posY > 1.02)
                transform.position = new Vector3(transform.position.x, borderY * -1);
            else if (posY < -0.02)
                transform.position = new Vector3(transform.position.x, borderY);
        }
    }
}
