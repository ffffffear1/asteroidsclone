﻿﻿using UnityEngine;

 namespace common.singleton
{
	public class Singleton<T> : MonoBehaviour where T: MonoBehaviour
	{
		private static T instance;
		
		public static T Instance
		{
			get
			{
				if (instance == null)
				{
					instance = FindObjectOfType<T>();
					if (instance == null)
					{
						var singleton = new GameObject();
						instance = singleton.AddComponent<T>();
						instance.name = typeof(T).ToString();

						DontDestroyOnLoad(instance);
					}
					DontDestroyOnLoad(instance);
				}

				return instance;
			}
		}
	}
}