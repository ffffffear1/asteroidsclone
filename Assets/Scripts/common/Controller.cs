﻿using common.singleton;
using UnityEngine;

namespace common
{
    public abstract class Controller : Singleton<Controller>
    {
        protected float movementSpeed, rotatedSpeed, shootingForse;
        protected Camera camera;
        protected Transform bulletSpawnPosition;

        protected void InIt(float movementSpeed, float rotatedSpeed, float shootingForse, Transform bulletSpawnPosition)
        {
            this.movementSpeed = movementSpeed;
            this.rotatedSpeed = rotatedSpeed;
            this.shootingForse = shootingForse;
            this.bulletSpawnPosition = bulletSpawnPosition;
            
            camera = Camera.main;
        }
        
        protected abstract void Move(float speed);
        protected abstract void Rotate(float dir);
        protected abstract void Shooting();
        protected internal abstract void Death();
    }
}