﻿using UnityEngine;

namespace common.objectPooling
{
    public class PoolSetup : MonoBehaviour
    {
        [SerializeField] private PoolManager.PoolPart[] pools;
        
        private void Awake() => InIt();
        private void InIt () => PoolManager.Instance.InIt(pools);
        private void OnValidate()
        {
            for (int i = 0; i < pools.Length; i++) { pools[i].name = pools[i].prefab.name; }
        }
    }
}