﻿﻿using UnityEngine;

 namespace common.objectPooling
{
    public class PoolObject : MonoBehaviour
    {
        public void ReturnToPool() => gameObject.SetActive(false);
    }
}
