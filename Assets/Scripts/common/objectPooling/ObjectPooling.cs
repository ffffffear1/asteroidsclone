﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace common.objectPooling
{
    public class ObjectPooling
    {
        private List<PoolObject> listObjects;

        public void InIt(int count, PoolObject sample, Transform parent)
        {
            listObjects = new List<PoolObject>();
            
            for (int i = 0; i < count; i++) { Add(sample, parent); }
        }

        public PoolObject GetObject()
        {
            foreach (var poolObj in listObjects.Where(poolObj => !poolObj.gameObject.activeSelf))
            {
                poolObj.gameObject.SetActive(true);
                return poolObj;
            }

            return null;
        }

        private void Add(PoolObject sample, Transform parent)
        {
            var newObject = GameObject.Instantiate(sample.gameObject, parent, true);
            newObject.name = sample.name;
            
            listObjects.Add(newObject.GetComponent<PoolObject>());
            newObject.SetActive(false);
        }
    }
}