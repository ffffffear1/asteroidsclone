﻿using System;
using common.singleton;
using UI;
using UnityEngine;

namespace common.objectPooling
{
    public class PoolManager : Singleton<PoolManager>
    {
        [HideInInspector] public GameObject parent;
        private PoolPart[] pools;
        
        public void InIt(PoolPart[] newPools)
        {
            pools = newPools;
            parent = new GameObject {name = "pool"};

            for (int i = 0; i < pools.Length; i++)
            {
                if (pools[i].prefab == null) continue;
                
                pools[i].ferula = new ObjectPooling();
                pools[i].ferula.InIt(pools[i].count, pools[i].prefab, parent.transform);
            }
        }

        public void CheckPool()
        {
            var endRound = true;
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                var child = parent.transform.GetChild(i).gameObject;
                
                if (!child.activeSelf) continue;
                if (!child.name.Contains("PlayerBullet") && !child.name.Contains("EnemyBullet"))
                {
                        
                    endRound = false;   
                }
            }
            if (endRound) StartCoroutine(UIController.Instance.NextRound());
        }

        public GameObject GetObject(string name, Vector2 position, Quaternion rotation)
        {
            if (pools != null)
            {
                for (int i = 0; i < pools.Length; i++)
                {
                    if (string.CompareOrdinal(pools[i].name, name).Equals(0))
                    {
                        var newObj = pools[i].ferula.GetObject();
                        if (newObj != null)
                        {
                            var result = newObj.gameObject;
                            result.transform.position = position;
                            result.transform.rotation = rotation;
                            return result;
                        }
                    }
                }
            }
            return null;
        }
        
        
        [Serializable]
        public struct PoolPart
        {
            public string name;
            public PoolObject prefab;
            public int count;
            public ObjectPooling ferula;
        }
    }
}