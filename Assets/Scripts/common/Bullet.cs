﻿using common.objectPooling;
using enemy.asteroidPackage;
using enemy.UFOPackage;
using UnityEngine;

namespace common
{
    public class Bullet : MonoBehaviour
    {
        private float time, timer = 4;

        private void Update()
        {
            time += Time.deltaTime;
            if (time > timer)
            {
                time = 0;
                GetComponent<PoolObject>().ReturnToPool();
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.tag)
            {
                case "UFO":
                    if (gameObject.name.Contains("PlayerBullet"))
                    {
                        other.GetComponent<UFO>().Death();
                        PoolManager.Instance.CheckPool();
                        GetComponent<PoolObject>().ReturnToPool();
                    }
                    break;
                case "Obstacle":
                    other.GetComponent<Asteroid>().Death();
                    PoolManager.Instance.CheckPool();
                    GetComponent<PoolObject>().ReturnToPool();
                    break;
            }
        }
    }
}
