﻿using System.Collections;
using common.objectPooling;
using common.singleton;
using playerPackage;
using UnityEngine;

namespace common
{
    public class Generator : Singleton<Generator>
    {
        [HideInInspector] public int spawnCountAsteoids;

        private void Awake() => spawnCountAsteoids = GameManager.Instance.GetCountFirstSpawnAsteroids;

        public void SpawnObstacle()
        {
            for (int i = 0; i < spawnCountAsteoids; i++)
            {
                PoolManager.Instance.GetObject("BigAsteroid", RandomPosition(i),
                    new Quaternion(0, 0, Random.Range(0, 360), 360));
            }
            spawnCountAsteoids += GameManager.Instance.GetIncreaseSpawnAsteroids;
        }

        private void SpawnUFO()
        {
            PoolManager.Instance.GetObject("UFO", RandomPosition(Random.Range(0, 3)),
                Quaternion.identity);
        }

        public IEnumerator TimerUFO(float timer)
        {
            while (true)
            {
                yield return new WaitForSeconds(timer);
                if(Player.Instance.gameObject != null)
                    SpawnUFO();
            }
        }

        private Vector2 RandomPosition(int count)
        {
            float randomMulti = 0, x = 0, y = 0;
            
            var random = new[] {-1, 1};
            randomMulti = random[Random.Range(0, 2)];
                
            x = Random.Range(-8, 8);    
            y = Random.Range(-4, 4);

            if (count % 2 == 0) x = Random.Range(16 * randomMulti, 8 * randomMulti);
            else y = Random.Range(4 * randomMulti, 8 * randomMulti);

            return new Vector2(x, y);
        }
    }
}
