﻿using System;
using System.Collections;
using common;
using common.singleton;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UIController : Singleton<UIController>
    {
        public GameObject startPanel, gamePlayPanel, nextRoundPanel, losePanel;
        public TextMeshProUGUI score, finalScore, roundCount;
        
        public Action<float> OnScoreChanged = v => { };

        private void Start() => OnScoreChanged += UpdateUI;
        private void UpdateUI(float score) => this.score.text = $"Score: {score}";
        public void StartGameButton() => GameManager.Instance.StartGame();
        public void RestartGameButton() => GameManager.Instance.RestartGame();
        
        public IEnumerator NextRound()
        {
            StopAllCoroutines();
            GameManager.Instance.RoundCount++;
            nextRoundPanel.SetActive(true);
            roundCount.text = $"Round {GameManager.Instance.RoundCount}";
            
            yield return new WaitForSeconds(3);

            StartCoroutine(Generator.Instance.TimerUFO(GameManager.Instance.GetRateSpawnUfo));
            nextRoundPanel.SetActive(false);
            Generator.Instance.SpawnObstacle();
        }
    }
}
