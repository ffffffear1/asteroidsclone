﻿using common;
using common.objectPooling;
using playerPackage;
using UnityEngine;

namespace enemy.UFOPackage
{
    public class UFO : ScreenLimitation
    {
        [SerializeField] private Transform shootingPos;

        private PoolObject poolObject;
        private Transform target;
        private GameManager gameManager;
        private float simpleShotDelay;

        private void Start()
        {
            poolObject = GetComponent<PoolObject>();
            target = FindObjectOfType<Player>().transform;

            gameManager = GameManager.Instance;

            InIt(
                0,
                0,
                gameManager.GetUfoShootingSpeed,
                shootingPos);
        }

        private void FixedUpdate()
        {
            if (target == null) return;
            
            CheckPosition();
            Move(movementSpeed);
            Shooting();
                
            if (Vector3.Distance(transform.position, target.position) > 2.5f)
                movementSpeed += gameManager.GetUfoIncreaseSpeed;
            else
                movementSpeed -= movementSpeed > 0 ? movementSpeed * 0.1f : 0;
        }

        protected override void Move(float speed) => transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        protected override void Rotate(float dir) { }
        protected override void Shooting() => SimpleShot();
        protected internal override void Death()
        {
            GameManager.Instance.Score += 250;
            poolObject.ReturnToPool();
        }

        private void SimpleShot()
        {
            if (simpleShotDelay <= 0)
            {
                simpleShotDelay = GameManager.Instance.GetUfoDelayShooting;
                var bullet = PoolManager.Instance.GetObject("EnemyBullet", bulletSpawnPosition.position, transform.rotation);
                var dir = target.position - transform.position;
                dir.Normalize();    
                bullet.GetComponent<Rigidbody2D>().AddForce(dir * shootingForse, ForceMode2D.Force);
            }

            simpleShotDelay -= Time.deltaTime;
        }
    }
}
