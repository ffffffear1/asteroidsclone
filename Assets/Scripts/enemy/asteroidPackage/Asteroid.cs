﻿using common;
using common.objectPooling;
using UnityEngine;

namespace enemy.asteroidPackage
{
    public class Asteroid : ScreenLimitation
    {
        [SerializeField] protected internal AsteroidType type;

        private PoolObject poolObject;
        private float asteroidSpeed;
        private float asteroidRotatedSpeed;

        private void Awake()
        {
            switch (type)
            {
                case AsteroidType.Big:
                    asteroidSpeed = GameManager.Instance.GetAsteroidMovementSpeed * 0.3f;
                    asteroidRotatedSpeed = GameManager.Instance.GetAsteroidRotatedSpeed * 0.3f;
                    break;
                case AsteroidType.Middle:
                    asteroidSpeed = GameManager.Instance.GetAsteroidMovementSpeed * 0.6f;
                    asteroidRotatedSpeed = GameManager.Instance.GetAsteroidRotatedSpeed * 0.6f;
                    break;
                case AsteroidType.Small:
                    asteroidSpeed = GameManager.Instance.GetAsteroidMovementSpeed;
                    asteroidRotatedSpeed = GameManager.Instance.GetAsteroidRotatedSpeed;
                    break;
            }
        }

        private void Start()
        {
            poolObject = GetComponent<PoolObject>();
            
            InIt(
                asteroidSpeed,
                asteroidRotatedSpeed,
                0,
                null);
        }

        private void FixedUpdate()
        {
            CheckPosition();
            
            Move(movementSpeed);
            Rotate(rotatedSpeed);
        }
        
        protected override void Move(float speed) => transform.Translate(Vector3.up * speed * Time.deltaTime);
        protected override void Rotate(float dir) => transform.Rotate(new Vector3(0, 0, dir * Time.deltaTime));
        protected override void Shooting() { }
        protected internal override void Death()
        {
            switch (type)
            {
                case AsteroidType.Big:
                    GameManager.Instance.Score += 50;
                    PoolManager.Instance.GetObject("MiddleAsteroid", transform.position, transform.rotation);
                    PoolManager.Instance.GetObject("MiddleAsteroid", transform.position, new Quaternion(0, 0, Random.Range(-90, 90), 360));
                    break;
                case AsteroidType.Middle:
                    GameManager.Instance.Score += 100;
                    PoolManager.Instance.GetObject("SmallAsteroid", transform.position, transform.rotation);
                    PoolManager.Instance.GetObject("SmallAsteroid", transform.position, new Quaternion(0, 0, Random.Range(-90, 90), 360));
                    break;
                case AsteroidType.Small:
                    GameManager.Instance.Score += 150;
                    break;
            }
            poolObject.ReturnToPool();
        }
    }
    
    public enum AsteroidType
    {
        Big,
        Middle,
        Small
    }
}
